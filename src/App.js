// vim: tabstop=2 shiftwidth=2 expandtab

import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import TodoList from './TodoList';

class App extends Component {
  state = {
    date: new Date(),
  }

  render() {
    return <div>
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1>Hello {this.props.name}</h1>
        <h2>Date: {this.state.date.toLocaleString()}</h2>
      </header>
      <TodoList />
    </div>;
  }

  tick() {
    this.setState({
      date: new Date()
    });
  }

  componentDidMount() {
    this.timer = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }
}

export default App;
