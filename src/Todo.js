// vim: tabstop=2 shiftwidth=2 expandtab

import React from 'react';

const Todo = ({ done, id, onDelete, onClick, children }) => (
  <a href="#" className="list-group-item list-group-item-action clearfix" onClick={onClick(id)}>
    <span
      style={{
        textDecoration: done ? "line-through" : "none"
      }}
    >{children}
    </span>
    <span
      className="btn btn-danger btn-md pull-right"
      onClick={e => {e.stopPropagation(); onDelete(id)(e);}}
    >Delete
    </span>
  </a>
);

export default Todo;
