// vim: tabstop=2 shiftwidth=2 expandtab

import React, { Component } from 'react';
import reactMixin from 'react-mixin';
import uniqueIdMixin from 'unique-id-mixin';

import { FormGroup, FormControl, ControlLabel } from 'react-bootstrap';

import Todo from './Todo';

class TodoList extends Component {
  state = {
    input: '',

    todos: [{
      id: Date.now(),
      text: 'Mocked TODO item',
      done: false
    }]
  }

  constructor(...args) {
    super(...args);

    // bind `this' to prototype's method
    //this.handleAddTodo = this.handleAddTodo.bind(this);
  }

  handleAddTodo = text => e => {
    e.preventDefault();

    this.setState(prevState => {
      return {
        input: '',
        todos: [...prevState.todos, {
          id: new Date(),
          text: text,
          done: false
        }]
      }
    });
  }

  handleInputChange = e => {
    const value = e.target.value;

    this.setState({
      input: value
    });
  }

  handleDeleteTodo = id => e => {
    this.setState(prevState => ({
      todos: prevState.todos.filter(todo => todo.id !== id)
    }));
  }

  handleCompleteTodo = id => e => {
    this.setState(prevState => {
      let todo = prevState.todos.find(todo => todo.id === id);
      todo.done = !todo.done;
      return {
        todos: prevState.todos
      }
    });
  }

  getValidationState() {
    const length = this.state.input.length;
    if (length > 10) return 'success';
    else if (length > 5) return 'warning';
    else if (length > 0) return 'error';
    return null;
  }

  render() {
    const { input, todos } = this.state;

    return <div className="Todo">
      <form onSubmit={this.handleAddTodo(input)}>
        <FormGroup controlId={this.getNextHtmlFor()} validationState={this.getValidationState()}>
          <ControlLabel>Type TODO text here</ControlLabel>
          <div className="input-group mb-3">
            <FormControl
              type="text"
              value={input}
              placeholder="Enter text"
              className="form-control"
              onChange={this.handleInputChange}
            />
            <div className="input-group-append">
              <button type="submit" className="btn btn-outline-secondary" disabled={!input.length} >Add</button>
            </div>
          </div>
        </FormGroup>
      </form>
      <div class="list-group">
        {todos.map(todo => (
          <Todo
            key={todo.id}
            {...todo}
            onDelete={this.handleDeleteTodo}
            onClick={this.handleCompleteTodo}
          >{todo.text}
          </Todo>
        ))}
      </div>
    </div>
  }
}

reactMixin(TodoList.prototype, uniqueIdMixin);

export default TodoList;
